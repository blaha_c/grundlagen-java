public class Fibonacci{
	public static void main(String[]args){
		for(int j = 1; j < 10; j++){
			System.out.println(iterativ(j));
			System.out.println(rekursiv(j));
		}
	}
	
	public static int iterativ(int a){
		int rueckgabe = 0; int b = 1; int d = 0;
		for(int i = 0; i < a; i++){
			if(a==1||a==2){
				return 1;
			}
			if(i == 0){
				rueckgabe = b + b;
				d = rueckgabe;
				i +=2;
			}
			else{
				rueckgabe=d + b;
				b = d;
				d = rueckgabe;
			}
		}
		return rueckgabe;
	}

	
	public static int rekursiv(int a){
		return (a==1||a==2)? 1:rekursiv(a-1)+rekursiv(a-2);
	}	
}	
